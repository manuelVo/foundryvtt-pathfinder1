_id: NP6VVchHAFS75Eh5
_key: '!items!NP6VVchHAFS75Eh5'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/violet_07.jpg
name: Demonic Vermin
system:
  changes:
    - _id: hdesxp5r
      formula: '2'
      target: nac
      type: untyped
    - _id: optcc9z5
      formula: '4'
      target: str
      type: untyped
    - _id: 0affessr
      formula: '2'
      target: con
      type: untyped
  crOffset: '1'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Both<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>Deskari’s influence upon the
      Worldwound’s verminous life cannot be ignored for long by visitors to this
      tainted land. While some giant vermin have resisted Deskari’s influence,
      most have succumbed to it. Such creatures are almost always encountered
      along the Worldwound’s periphery. As one travels deeper into the blighted
      land, the immense insects, spiders, and other vermin encountered in the
      canyons and rivers take on an increasingly unsettling intelligence and
      demonic features. In many ways, these demonic vermin are no longer true
      denizens of the Material Plane—they are, after a fact, what happens when
      the chaos and evil of the Abyss infuse a mindless creature. They are the
      unholy spawn of vermin—and mortal sins.<p>Countless variations of demonic
      vermin exist in the forbidding wasteland that is the Worldwound. When a
      nest of similar monsters is encountered, they all typically share the same
      demonic powers and traits, but another nest of the same species could
      exhibit entirely different abilities, depending on the nature of the
      Abyssal energies that have corrupted and transformed them. Uncorrupted
      giant vermin that wander into or are otherwise brought into the Worldwound
      do not immediately fall victim to this vile transformation, but several
      months of exposure can, at the GM’s whim, cause such creatures to
      spontaneously transform into one of these hideous
      monstrosities.<p>"Demonic vermin" is an inherited or acquired template
      that can be added to any vermin (hereafter referred to as the base
      creature). A demonic vermin retains all the base creature’s statistics and
      special abilities except as noted here.<p><b>CR:</b> Same as the base
      creature +1.</p>

      <p><b>Alignment:</b> Chaotic evil.</p>

      <p><b>Type:</b> The creature’s type changes to magical beast. Do not
      recalculate HD, BAB, or saves. While a demonic vermin is not an outsider,
      it is treated as if it had the demon subtype for the purposes of resolving
      all effects relating to that subtype.</p>

      <p><b>Armor Class:</b> Natural armor improves by +2.</p>

      <p><b>Special Qualities and Defenses:</b> A demonic vermin gains immunity
      to electricity and poison and resistance to acid 10, cold 10, and fire 10.
      It also gains DR 5/cold iron (if 11 HD or less) or DR 10/cold iron (if 12
      HD or more). As demonic vermin are intelligent, they lose the mindless
      trait.</p>

      <p><b>Melee:</b> A demonic vermin’s natural weapons are unchanged, but
      they are treated as chaotic and evil for the purpose of resolving damage
      reduction.</p>

      <p><b>Special Attacks:</b> A demonic vermin retains all the special
      attacks of the base creature. In addition, it gains one of the following
      special abilities of your choosing— you can, of course, invent different
      abilities of your own as well. The save DC for any of these attacks is
      equal to 10 + 1/2 the demonic vermin’s HD + the demonic vermin’s
      Constitution modifier.</p>

      <p><em>Abyssal Energy (Su): </em>Choose one of the following energy
      types—acid, fire, or cold. The demonic vermin gains immunity to that
      energy type, and also gains a breath weapon that inflicts that type of
      energy damage. This breath weapon is a 60-foot-line, and deals 1d6 points
      of damage per CR point possessed by the demonic vermin (Reflex save
      halves). It can be used once every 1d4 rounds.</p>

      <p><em>Additional Senses (Ex):</em> The vermin has a large number of extra
      eyes and other sensory organs. It gains all-around vision, scent, and a +8
      racial bonus on Perception checks.</p>

      <p><em>Death Throes (Su): </em>When the vermin is slain, it can make a
      single melee attack (using any one of its natural attacks) as an immediate
      action. It then explodes into acid, fire, electricity, or cold (your
      choice), dealing 1d6 points of damage per CR point possessed by the
      demonic vermin (Reflex save halves).</p>

      <p><em>Diseased (Su): </em>The demonic vermin is immune to disease, and
      its natural attacks inflict demonplague (see page 29) on a hit (Fortitude
      save negates).</p>

      <p><em>Drone (Su): </em>By rubbing its wings or limbs together as a
      standard action, the demonic vermin produces a loud, discordant drone that
      causes those within 30 feet of it to become sickened (if the vermin is CR
      8 or less) or confused (if the vermin is CR 9 or higher) for 1d6 rounds
      (Will save negates). This is a sonic mind-affecting effect.</p>

      <p><em>Skitter (Ex): </em>The creature has uncanny speed and erratic
      movements. The vermin’s speeds all increase by 10 feet, it gains Mobility
      and Spring Attack as bonus feats, and it gains a +4 racial bonus on
      Initiative checks.</p>

      <p><b>Spell-Like Abilities:</b> In addition to gaining one of the special
      attacks listed here, all demonic vermin gain access to a limited number of
      spell-like abilities, depending on its Hit Dice. Each ability is usable
      once per day. Caster level equals the creature’s CR.</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>CR</b></td>

      <td><b>Abilities</b></td>

      </tr>

      <tr>

      <td>1–4</td>

      <td><em>darkness</em></td>

      </tr>

      <tr>

      <td>5–8</td>

      <td><em>vomit swarm</em><sup>APG</sup></td>

      </tr>

      <tr>

      <td>9–12</td>

      <td><em>insect plague</em></td>

      </tr>

      <tr>

      <td>13–16</td>

      <td><em>greater teleport</em> (self plus 50 lbs. of objects only)</td>

      </tr>

      <tr>

      <td>17–20</td>

      <td><em>earthquake</em></td>

      </tr>

      </tbody>

      </table>

      <hr>

      <p><br><b>Abilities:</b> Str +4, Con +2. A demonic vermin’s Intelligence
      becomes 10 and its Charisma becomes 15 (unless the base creature has
      higher values, in which case they remain unchanged).<br>Feats: A demonic
      vermin gains feats as appropriate for its Hit Dice, and gains Toughness as
      a bonus feat.</p>

      <p><b>Skills:</b> A demonic vermin has skill points per racial Hit Die
      equal to 4 + its Intelligence modifier. The following are class skills for
      demonic vermin: Acrobatics, Bluff, Climb, Fly, Knowledge (planes),
      Perception, Sense Motive, and Stealth.</p>

      <p><b>Languages:</b> A demonic vermin speaks Abyssal, Celestial, and
      Draconic. It also gains telepathy to a distance of 100 feet.</p>
  subType: template
type: feat
