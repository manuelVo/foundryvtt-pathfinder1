_id: y4b1zmhmsa0gvf06
_key: '!items!y4b1zmhmsa0gvf06'
_stats:
  coreVersion: '12.331'
folder: Ix6Dc5gXEobiIHKD
img: icons/magic/control/sihouette-hold-beam-green.webp
name: Planar Binding, Lesser
system:
  actions:
    - _id: jbgij5gk4xopv3w6
      actionType: spellsave
      activation:
        cost: 10
        type: minute
        unchained:
          cost: 10
          type: minute
      duration:
        units: inst
      name: Use
      range:
        units: close
      save:
        description: Will negates
        type: will
      target:
        value: one elemental or outsider with 6 HD or less
  components:
    somatic: true
    verbal: true
  description:
    value: >-
      <p>Casting this spell attempts a dangerous act: to lure a creature from
      another plane to a specifically prepared trap, which must lie within the
      spell's range. The called creature is held in the trap until it agrees to
      perform one service in return for its freedom.</p><p>To create the trap,
      you must use a <i>magic circle</i> spell, focused inward. The kind of
      creature to be bound must be known and stated. If you wish to call a
      specific individual, you must use that individual's proper name in casting
      the spell.</p><p>The target creature is allowed a Will saving throw. If
      the saving throw succeeds, the creature resists the spell. If the saving
      throw fails, the creature is immediately drawn to the trap (spell
      resistance does not keep it from being called). The creature can escape
      from the trap by successfully pitting its spell resistance against your
      caster level check, by dimensional travel, or with a successful Charisma
      check (DC 15 + 1/2 your caster level + your Charisma modifier). It can try
      each method once per day. If it breaks loose, it can flee or attack you. A
      <i>dimensional anchor</i> cast on the creature prevents its escape via
      dimensional travel. You can also employ a calling diagram (see <i>magic
      circle</i> against evil) to make the trap more secure.</p><p>If the
      creature does not break free of the trap, you can keep it bound for as
      long as you dare. You can attempt to compel the creature to perform a
      service by describing the service and perhaps offering some sort of
      reward. You make a Charisma check opposed by the creature's Charisma
      check. The check is assigned a bonus of +0 to +6 based on the nature of
      the service and the reward. If the creature wins the opposed check, it
      refuses service. New offers, bribes, and the like can be made or the old
      ones reoffered every 24 hours. This process can be repeated until the
      creature promises to serve, until it breaks free, or until you decide to
      get rid of it by means of some other spell. Impossible demands or
      unreasonable commands are never agreed to. If you ever roll a natural 1 on
      the Charisma check, the creature breaks free of the spell's effect and can
      escape or attack you.</p><p>Once the requested service is completed, the
      creature need only to inform you to be instantly sent back whence it came.
      The creature might later seek revenge. If you assign some open-ended task
      that the creature cannot complete through its own actions, the spell
      remains in effect for a maximum of 1 day per caster level, and the
      creature gains an immediate chance to break free (with the same chance to
      resist as when it was trapped). Note that a clever recipient can subvert
      some instructions.</p><p>When you use a calling spell to call an air,
      chaotic, earth, evil, fire, good, lawful, or water creature, it is a spell
      of that type.</p>
  descriptors:
    - see text
  learnedAt:
    class:
      arcanist: 5
      medium: 4
      occultist: 5
      psychic: 5
      sorcerer: 5
      summoner: 4
      summonerUnchained: 5
      wizard: 5
    domain:
      Rune: 5
      Void: 4
  level: 5
  school: con
  sources:
    - id: PZO1110
      pages: '321'
  sr: false
  subschool:
    - calling
type: spell
