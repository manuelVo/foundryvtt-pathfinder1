_id: ptqu8gtrujxsvwy1
_key: '!items!ptqu8gtrujxsvwy1'
_stats:
  coreVersion: '12.331'
folder: Ix6Dc5gXEobiIHKD
img: icons/environment/settlement/tavern.webp
name: Teleport
system:
  actions:
    - _id: jfofxpkuq1x5tccy
      actionType: spellsave
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      duration:
        units: inst
      name: Use
      range:
        units: personal
      save:
        description: none and Will negates (object)
        type: will
      target:
        value: you and touched objects or other touched willing creatures
  components:
    verbal: true
  description:
    value: >-
      <p>This spell instantly transports you to a designated destination, which
      may be as distant as 100 miles per caster level. Interplanar travel is not
      possible. You can bring along objects as long as their weight doesn't
      exceed your maximum load. You may also bring one additional willing Medium
      or smaller creature (carrying gear or objects up to its maximum load) or
      its equivalent per three caster levels. A Large creature counts as two
      Medium creatures, a Huge creature counts as four Medium creatures, and so
      forth. All creatures to be transported must be in contact with one
      another, and at least one of those creatures must be in contact with you.
      As with all spells where the range is personal and the target is you, you
      need not make a saving throw, nor is spell resistance applicable to you.
      Only objects held or in use (attended) by another person receive saving
      throws and spell resistance.</p><p>You must have some clear idea of the
      location and layout of the destination. The clearer your mental image, the
      more likely the teleportation works. Areas of strong physical or magical
      energy may make teleportation more hazardous or even impossible.</p><p>To
      see how well the teleportation works, roll d% and consult the table at the
      end of this spell. Refer to the following information for definitions of
      the terms on the table.</p><p><em>Familiarity:</em> "Very familiar" is a
      place where you have been very often and where you feel at home. "Studied
      carefully" is a place you know well, either because you can currently
      physically see it or you've been there often. "Seen casually" is a place
      that you have seen more than once but with which you are not very
      familiar. "Viewed once" is a place that you have seen once, possibly using
      magic such as <em>scrying.</em></p><p>"False destination" is a place that
      does not truly exist or if you are teleporting to an otherwise familiar
      location that no longer exists as such or has been so completely altered
      as to no longer be familiar to you. When traveling to a false destination,
      roll 1d20+80 to obtain results on the table, rather than rolling d%([[/r
      d100]]), since there is no real destination for you to hope to arrive at
      or even be off target from.</p><p><em>On Target</em>: You appear where you
      want to be.</p><p><em>Off Target</em>: You appear safely a random distance
      away from the destination in a random direction. Distance off target is d%
      of the distance that was to be traveled. The direction off target is
      determined randomly.</p><p><em>Similar Area</em>: You wind up in an area
      that's visually or thematically similar to the target area. Generally, you
      appear in the closest similar place within range. If no such area exists
      within the spell's range, the spell simply fails
      instead.</p><p><em>Mishap:</em> You and anyone else teleporting with you
      have gotten "scrambled." You each take 1d10 points of damage, and you
      reroll on the chart to see where you wind up. For these rerolls, roll
      1d20+80.</p><p>Each time "Mishap" comes up, the characters take more
      damage and must
      reroll.</p><table><tbody><tr><th><p>Familiarity</p></th><th><p>On
      Target</p></th><th><p>Off Target</p></th><th><p>Similar
      Area</p></th><th><p>mishap</p></th></tr><tr><td><p>Very
      familiar</p></td><td><p>01-97</p></td><td><p>98-99</p></td><td><p>100</p></td><td><p>-</p></td></tr><tr><td><p>Studied
      carefully</p></td><td><p>01-94</p></td><td><p>95-97</p></td><td><p>98-99</p></td><td><p>100</p></td></tr><tr><td><p>Seen
      casually</p></td><td><p>01-88</p></td><td><p>89-94</p></td><td><p>95-98</p></td><td><p>99-100</p></td></tr><tr><td><p>Viewed
      once</p></td><td><p>01-76</p></td><td><p>77-88</p></td><td><p>89-96</p></td><td><p>97-100</p></td></tr><tr><td><p>False
      destination</p></td><td><p>-</p></td><td><p>-</p></td><td><p>81-92</p></td><td><p>93-100</p></td></tr></tbody></table>
  learnedAt:
    class:
      arcanist: 5
      magus: 5
      medium: 4
      occultist: 5
      psychic: 5
      sorcerer: 5
      spiritualist: 5
      summoner: 4
      summonerUnchained: 5
      witch: 5
      wizard: 5
    domain:
      Travel: 5
  level: 5
  school: con
  sources:
    - id: PZO1110
      pages: 358, 359
  sr: false
  subschool:
    - teleportation
type: spell
