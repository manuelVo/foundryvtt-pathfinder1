_id: EbgtpUXqVup5iXeD
_key: '!items!EbgtpUXqVup5iXeD'
_stats:
  coreVersion: '12.331'
img: icons/creatures/birds/corvid-watchful-glowing-green.webp
name: Familiar
system:
  changes:
    - _id: ZIhKRP5T
      formula: 5 + ceil(@classes.familiar.level / 2)
      operator: set
      target: int
      type: untyped
    - _id: nw0v2aFf
      formula: ceil(@classes.familiar.level / 2)
      target: nac
      type: untyped
  classSkills:
    acr: true
    clm: true
    fly: true
    per: true
    ste: true
    swm: true
  description:
    value: >-
      <p>A familiar is an animal chosen by a spellcaster to aid him in his study
      of magic. It retains the appearance, Hit Dice, base attack bonus, base
      save bonuses, skills, and feats of the normal animal it once was, but is
      now a magical beast for the purpose of effects that depend on its type.
      Only a normal, unmodified animal may become a familiar. An animal
      companion cannot also function as a familiar.</p><p>A familiar grants
      special abilities to its master, as given on the table below. These
      special abilities apply only when the master and familiar are within 1
      mile of each other.</p><p>Levels of different classes that are entitled to
      familiars stack for the purpose of determining any familiar abilities that
      depend on the master’s level.</p><p>If a familiar is dismissed, lost, or
      dies, it can be replaced 1 week later through a specialized ritual that
      costs 200 gp per wizard level. The ritual takes 8 hours to
      complete.</p><h2>Familiar Basics</h2><p>Use the basic statistics for a
      creature of the familiar's kind, but with the following
      changes.</p><p><strong>Hit Dice</strong>: For the purpose of effects
      related to number of Hit Dice, use the master's character level or the
      familiar's normal HD total, whichever is higher.</p><p><strong>Hit
      Points</strong>: The familiar has half the master's total hit points (not
      including temporary hit points), rounded down, regardless of its actual
      Hit Dice.</p><p><strong>Attacks</strong>: Use the master's base attack
      bonus, as calculated from all his classes. Use the familiar's Dexterity or
      Strength modifier, whichever is greater, to calculate the familiar's melee
      attack bonus with natural weapons. Damage equals that of a normal creature
      of the familiar's kind.</p><p><strong>Saving Throws</strong>: For each
      saving throw, use either the familiar's base save bonus (Fortitude +2,
      Reflex +2, Will +0) or the master's (as calculated from all his classes),
      whichever is better. The familiar uses its own ability modifiers to saves,
      and it doesn't share any of the other bonuses that the master might have
      on saves.</p><p><strong>Skills</strong>: For each skill in which either
      the master or the familiar has ranks, use either the normal skill ranks
      for an animal of that type or the master's skill ranks, whichever is
      better. In either case, the familiar uses its own ability modifiers.
      Regardless of a familiar's total skill modifiers, some skills may remain
      beyond the familiar's ability to use. Familiars treat Acrobatics, Climb,
      Fly, Perception, Stealth, and Swim as class skills.</p><hr
      /><p><strong>Usage</strong>: Setup BAB, Saving Throws and Hit Points in
      Details tab according to familiar and master stats as mentioned
      above</p><hr /><h2>Familiar Ability Descriptions</h2><p>All familiars have
      special abilities (or impart abilities to their masters) depending on the
      master's combined level in classes that grant familiars, as shown on the
      table below. The abilities are cumulative.</p><table><thead><tr><th
      style="width:15%">Master Class Level</th><th style="width:15%">Natural
      Armor Adj.</th><th
      style="width:5%">Int</th><th>Special</th></tr></thead><tbody><tr><td>1st–2nd</td><td>+1</td><td>6</td><td>@UUID[Compendium.pf1.companion-features.Item.iYSmTvVIXZt2uMov]{Alertness},
      @UUID[Compendium.pf1.companion-features.Item.ooxP3ifhV6kC0wTM]{improved
      evasion},
      @UUID[Compendium.pf1.companion-features.Item.B4tsrPOAtyEOmwAr]{share
      spells},
      @UUID[Compendium.pf1.companion-features.Item.YtzidlXTKvVyiNcY]{empathic
      link}</td></tr><tr><td>3rd–4th</td><td>+2</td><td>7</td><td>@UUID[Compendium.pf1.companion-features.Item.XttM5JcGilkhdFCW]{Deliver
      touch
      spells}</td></tr><tr><td>5th–6th</td><td>+3</td><td>8</td><td>@UUID[Compendium.pf1.companion-features.Item.GgqfqGq8vopOdN32]{Speak
      with
      master}</td></tr><tr><td>7th–8th</td><td>+4</td><td>9</td><td>@UUID[Compendium.pf1.companion-features.Item.yNMQccEicirDocG7]{Speak
      with animals of its
      kind}</td></tr><tr><td>9th–10th</td><td>+5</td><td>10</td><td>—</td></tr><tr><td>11th–12th</td><td>+6</td><td>11</td><td>@UUID[Compendium.pf1.companion-features.Item.OrbV3Q6U2BdnL8tR]{Spell
      resistance}</td></tr><tr><td>13th–14th</td><td>+7</td><td>12</td><td>@UUID[Compendium.pf1.companion-features.Item.mUPO7LpDVJtOoP4W]{Scry
      on
      familiar}</td></tr><tr><td>15th–16th</td><td>+8</td><td>13</td><td>—</td></tr><tr><td>17th–18th</td><td>+9</td><td>14</td><td>—</td></tr><tr><td>19th–20th</td><td>+10</td><td>15</td><td>—</td></tr></tbody></table><p><strong>Natural
      Armor Adj</strong>.: The number noted here is in addition to the
      familiar's existing natural armor bonus.</p><p><strong>Int</strong>: The
      familiar's Intelligence score.</p>
  hp: null
  links:
    classAssociations:
      - level: 1
        uuid: Compendium.pf1.companion-features.Item.iYSmTvVIXZt2uMov
      - level: 1
        uuid: Compendium.pf1.companion-features.Item.YtzidlXTKvVyiNcY
      - level: 1
        uuid: Compendium.pf1.companion-features.Item.ooxP3ifhV6kC0wTM
      - level: 1
        uuid: Compendium.pf1.companion-features.Item.B4tsrPOAtyEOmwAr
      - level: 3
        uuid: Compendium.pf1.companion-features.Item.XttM5JcGilkhdFCW
      - level: 5
        uuid: Compendium.pf1.companion-features.Item.GgqfqGq8vopOdN32
      - level: 7
        uuid: Compendium.pf1.companion-features.Item.yNMQccEicirDocG7
      - level: 11
        uuid: Compendium.pf1.companion-features.Item.OrbV3Q6U2BdnL8tR
      - level: 13
        uuid: Compendium.pf1.companion-features.Item.mUPO7LpDVJtOoP4W
  sources:
    - id: PZO1110
      pages: '83'
  tag: familiar
type: class
