_id: rz4xnCDMtAWGzSBR
_key: '!items!rz4xnCDMtAWGzSBR'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/green_12.jpg
name: Pestilence Bloodline
system:
  associations:
    classes:
      - Sorcerer
  classSkills:
    hea: true
  description:
    value: >-
      <p><em>You were born during the height of a great magical plague, to a
      mother suffering from an eldritch disease, or you suffered an eldritch pox
      as a child, such that your very soul now carries a blight of pestilence
      within it.</em></p><p><strong>Class Skill:</strong>
      <em>Heal</em>.</p><p><strong>Bonus
      Spells:</strong></p><ul><li>@Compendium[pf1.spells.pg7dbmuuaksxhp3v]{Charm
      Animal} (3rd)</li><li>@Compendium[pf1.spells.vxi9c3xwa83xthka]{Summon
      Swarm} (5th)</li><li>@Compendium[pf1.spells.ppeb97nxg1rmdpme]{Contagion}
      (7th)</li><li>@Compendium[pf1.spells.zonwg3yt8pswj03y]{Repel Vermin}
      (9th)</li><li>@Compendium[pf1.spells.jdp0mts5razk0qms]{Insect Plague}
      (11th)</li><li>@Compendium[pf1.spells.z8xnkfpufuqapseq]{Eyebite}
      (13th)</li><li>@Compendium[pf1.spells.f828mjoo5afszqnk]{Creeping Doom}
      (15th)</li><li>@Compendium[pf1.spells.e8zen5nzixnt7bde]{Horrid Wilting}
      (17th)</li><li>@Compendium[pf1.spells.51ob5y7phwlhcuzz]{Power Word Kill}
      (19th)</li></ul><p><strong>Bonus
      Feats:</strong></p><ul><li>@Compendium[pf1.feats.nNVFVFRBCT4O2ab9]{Brew
      Potion}</li><li>@Compendium[pf1.feats.O0e0UCim27GPKFuW]{Diehard}</li><li>@Compendium[pf1.feats.ehqx8txNRGMaNOPt]{Endurance}</li><li>@Compendium[pf1.feats.fVRnBIHR4Jq6btvm]{Great
      Fortitude}</li><li>@Compendium[pf1.feats.8VZQJOyAXbXghxd7]{Self-Sufficient}</li><li>@Compendium[pf1.feats.7zJTztNW7hdaqjKc]{Skill
      Focus} (<em>Knowledge</em>
      [nature])</li><li>@Compendium[pf1.feats.2YuxcUNFMPNBpekG]{Silent
      Spell}</li><li>@Compendium[pf1.feats.8snLqsJN4LLL00Nq]{Toughness}</li></ul><p><strong>Bloodline
      Arcana:</strong> <em>Vermin</em> are susceptible to your mind-affecting
      spells. They are treated as <em>animals</em> for the purposes of
      determining which mind-affecting spells affect
      them.</p><p><strong>Bloodline Powers</strong>: You awaken and quicken the
      lurking pestilence in your own body or the surrounding world to wreak
      hideous malice, or to command and commune with agents of such
      plagues.</p><p>@UUID[Compendium.pf1.class-abilities.Item.19Fh7slWwEfavrod]{Plague's
      Caress (Sp)}: At 1st level, you can make a melee touch attack as a
      standard action that causes a living creature’s flesh to break out into
      rancid-smelling pustules and sores for a number of rounds equal to 1/2
      your sorcerer level (minimum 1 round). These sores cause the victim to
      become sickened for the duration of the effect; this is a disease effect.
      You can use this ability a number of times per day equal to 3 + your
      Charisma
      modifier.</p><p>@UUID[Compendium.pf1.class-abilities.Item.pUEihPkSss2Jxgdr]{Accustomed
      to Awfulness (Ex)}: At 3rd level, you become immune to the sickened
      condition and gain a +4 bonus on all saving throws against effects that
      cause nausea or disease. At 9th level, you become immune to the nauseated
      condition and to the debilitating effects of disease (but you can still be
      a carrier of
      diseases).</p><p>@UUID[Compendium.pf1.class-abilities.Item.LBrnB5Ncpcb5oryt]{Shroud
      of Vermin (Su)}: At 9th level, swarms no longer see you as prey. You can
      walk among swarms without fear of being harmed by them at all, and by
      taking a standard action to mentally command a swarm in which you stand,
      you can direct that swarm’s attacks and movements as long as you have more
      Hit Dice than the swarm. Even when you aren’t standing amid a swarm, your
      body crawls with vermin, and their chitinous bodies increase your natural
      armor bonus by +1. At 11th level, this bonus increases to +2, and at 17th
      level it increases to
      +3.</p><p>@UUID[Compendium.pf1.class-abilities.Item.IhSn8BMyjnIzLxiH]{Pestilential
      Breath (Su)}: At 15th level, the sickness within your body finally becomes
      so potent that your very breath is deadly. Once per day as a standard
      action, you can exhale a cloud of pestilence in a 30-foot cone. Those
      caught in the area of this miasmic cloud receive a single Fortitude save
      to avoid suffering the effects of two different diseases. The DC of this
      save is equal to 10 + 1/2 your sorcerer level + your Charisma modifier.
      You can choose what two diseases you inflict on each target that succumbs
      to your plague breath, but they must be two different diseases chosen from
      the following list: blinding sickness, bubonic plague, cackle fever, filth
      fever, leprosy, mindfire, red ache, shakes, or slimy doom. The victim
      suffers the initial effects of these two diseases immediately — use the
      diseases’ frequency and save DC normally to determine further effects as
      detailed on page 557 of the Pathfinder RPG Core Rulebook. At 17th level,
      you can use this ability twice per day. At 20th level you can use this
      ability three times per
      day.</p><p>@UUID[Compendium.pf1.class-abilities.Item.kXa1d6pYxY8Sd2BD]{Plague
      Carrier (Su)}: At 20th level, your touch inflicts mummy rot on those you
      strike. You can choose to suppress this ability for 1 round as a swift
      action. You can make a touch attack to inflict this disease on a target,
      or transfer it as part of an attack with any melee weapon or touch-based
      spell. The creature touched can resist contracting mummy rot by making a
      Fortitude save - the DC is equal to 10 + 1/2 your sorcerer level + your
      Charisma modifier.</p>
  sources:
    - id: PZO9029
      pages: '29'
  subType: classFeat
type: feat
