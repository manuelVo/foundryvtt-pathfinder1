_id: Drj7Yp0Ymke4lFZb
_key: '!items!Drj7Yp0Ymke4lFZb'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/shadow_13.jpg
name: Swashbuckler Deeds
system:
  associations:
    classes:
      - Swashbuckler
  description:
    value: >-
      <p><em>Derring-Do (Ex):</em> At 1st level, a swashbuckler can spend 1
      panache point when she makes an <em>Acrobatics</em>, <em>Climb</em>,
      <em>Escape Artist</em>, <em>Fly</em>, <em>Ride</em>, or <em>Swim</em>
      check to roll 1d6 and add the result to the check. She can do this after
      she makes the check but before the result is revealed. If the result of
      the d6 roll is a natural 6, she rolls another 1d6 and adds it to the
      check. She can continue to do this as long as she rolls natural 6s, up to
      a number of times equal to her <em>Dexterity</em> modifier (minimum
      1).</p>

      <p><em>Dodging Panache (Ex):</em> At 1st level, when an opponent attempts
      a melee attack against the swashbuckler, the swashbuckler can as an
      <em>immediate action</em> spend 1 panache point to move 5 feet; doing so
      grants the swashbuckler a <em>dodge bonus</em> to AC equal to her
      <em>Charisma</em> modifier (minimum 0) against the triggering attack. This
      movement doesn’t negate the attack, which is still resolved as if the
      swashbuckler had not moved from the original square. This movement is not
      a 5-foot step; it provokes <em>attacks of opportunity</em> from creatures
      other than the one who triggered this deed. The swashbuckler can only
      perform this deed while wearing light or no <em>armor</em>, and while
      carrying no heavier than a light load.</p>

      <p><em>Opportune Parry and Riposte (Ex):</em> At 1st level, when an
      opponent makes a melee attack against the swashbuckler, she can spend 1
      panache point and expend a use of an <em>attack of opportunity</em> to
      attempt to parry that attack. The swashbuckler makes an <em>attack
      roll</em> as if she were making an <em>attack of opportunity</em>; for
      each size category the attacking creature is larger than the swashbuckler,
      the swashbuckler takes a –2 penalty on this roll. If her result is greater
      than the attacking creature’s result, the creature’s attack automatically
      misses. The swashbuckler must declare the use of this ability after the
      creature’s attack is announced, but before its <em>attack roll</em> is
      made. Upon performing a successful parry and if she has at least 1 panache
      point, the swashbuckler can as an <em>immediate action</em> make an attack
      against the creature whose attack she parried, provided that creature is
      within her reach. This deed’s cost cannot be reduced by any ability or
      effect that reduces the number of panache points a deed costs.</p>

      <p><em>Kip-Up (Ex):</em> At 3rd level, while the swashbuckler has at least
      1 panache point, she can kip-up from <em>prone</em> as a <em>move
      action</em> without provoking an <em>attack of opportunity</em>. She can
      kip-up as a <em>swift action</em> instead by spending 1 panache point.</p>

      <p><em>Menacing Swordplay (Ex): </em>At 3rd level, while she has at least
      1 panache point, when a swashbuckler hits an opponent with a light or
      one-handed piercing melee weapon, she can choose to use
      <em>Intimidate</em> to demoralize that opponent as a <em>swift action</em>
      instead of a <em>standard action</em>.</p>

      <p><em>Precise Strike (Ex):</em> At 3rd level, while she has at least 1
      panache point, a swashbuckler gains the ability to strike precisely with a
      light or one-handed piercing melee weapon (though not <em>natural
      weapon</em> attacks), adding her swashbuckler level to the damage dealt.
      To use this deed, a swashbuckler cannot attack with a weapon in her other
      hand or use a shield other than a buckler. She can even use this ability
      with thrown light or one-handed piercing melee weapons, so long as the
      target is within 30 feet of her. Any creature that is immune to <em>sneak
      attacks</em> is immune to the additional damage granted by precise strike,
      and any item or ability that protects a creature from critical hits also
      protects a creature from the additional damage of a precise strike. This
      additional damage is precision damage, and isn’t multiplied on a
      <em>critical hit</em>. As a <em>swift action</em>, a swashbuckler can
      spend 1 panache point to double her precise strike’s damage bonus on the
      next attack. This benefit must be used before the end of her turn, or it
      is lost. This deed’s cost cannot be reduced by any ability or effect that
      reduces the amount of panache points a deed costs (such as the
      <em>Signature Deed</em> feat).</p>

      <p><em>Swashbuckler Initiative (Ex):</em> At 3rd level, while the
      swashbuckler has at least 1 panache point, she gains a +2 bonus on
      <em>initiative</em> checks. In addition, if she has the <em>Quick
      Draw</em> feat, her hands are free and unrestrained, and she has any
      single light or one-handed piercing melee weapon that isn’t hidden, she
      can draw that weapon as part of the <em>initiative</em> check.</p>

      <p><em>Swashbuckler’s Grace (Ex):</em> At 7th level, while the
      swashbuckler has at least 1 panache point, she takes no penalty for moving
      at full speed when she uses <em>Acrobatics</em> to attempt to move through
      a <em>threatened</em> area or an enemy’s space.</p>

      <p><em>Superior Feint (Ex): </em>At 7th level, a swashbuckler with at
      least 1 panache point can, as a <em>standard action</em>, purposefully
      miss a creature she could make a melee attack against with a wielded light
      or one-handed piercing weapon. When she does, the creature is denied its
      <em>Dexterity</em> bonus to AC until the start of the swashbuckler’s next
      turn.</p>

      <p><em>Targeted Strike (Ex):</em> At 7th level, as a <em>full-round
      action</em> the swashbuckler can spend 1 panache point to make an attack
      with a single light or one-handed piercing melee weapon that cripples part
      of a foe’s body. The swashbuckler chooses a part of the body to target. If
      the attack succeeds, in addition to the attack’s normal damage, the target
      suffers one of the following effects based on the part of the body
      targeted. If a creature doesn’t have one of the listed body locations,
      that body part cannot be targeted. Creatures that are immune to <em>sneak
      attacks</em> are also immune to targeted strikes. Items or abilities that
      protect a creature from critical hits also protect a creature from
      targeted strikes.</p>

      <ul>

      <li><em>Arms:</em> The target takes no damage from the attack, but it
      drops one carried item of the swashbuckler’s choice, even if the item is
      wielded with two hands. Items held in a locked <em>gauntlet</em> cannot be
      chosen.</li>

      <li><em>Head:</em> The target is <em>confused</em> for 1 round. This is a
      <em>mind-affecting</em> effect.</li>

      <li><em>Legs:</em> The target is knocked <em>prone</em>. Creatures with
      four or more legs or that are immune to <em>trip</em> attacks are immune
      to this effect.</li>

      <li><em>Torso or Wings:</em> The target is <em>staggered</em> for 1
      round.</li>

      </ul>

      <p><em>Bleeding Wound (Ex):</em> At 11th level, when the swashbuckler hits
      a living creature with a light or one-handed piercing melee weapon attack,
      as a <em>free action</em> she can spend 1 panache point to have that
      attack deal additional <em>bleed</em> damage. The amount of <em>bleed</em>
      damage dealt is equal to the swashbuckler’s <em>Dexterity</em> modifier
      (minimum 1). Alternatively, the swashbuckler can spend 2 panache points to
      deal 1 point of <em>Strength</em>, <em>Dexterity</em>, or <em>Constitution
      bleed</em> damage instead (swashbuckler’s choice). Creatures that are
      immune to <em>sneak attacks</em> are also immune to these types of
      <em>bleed</em> damage.</p>

      <p><em>Evasive (Ex):</em> At 11th level, while a swashbuckler has at least
      1 panache point, she gains the benefits of the <em>evasion</em>,
      <em>uncanny dodge</em>, and <em>improved uncanny dodge rogue</em> class
      features. She uses her swashbuckler level as her <em>rogue</em> level for
      <em>improved uncanny dodge</em>.</p>

      <p><em>Subtle Blade (Ex): </em>At 11th level, while a swashbuckler has at
      least 1 panache point, she is immune to <em>disarm</em>, steal, and
      <em>sunder combat maneuvers</em> made against a light or one-handed
      piercing melee weapon she is wielding.</p>

      <p><em>Dizzying Defense (Ex):</em> At 15th level, while wielding a light
      or one-handed piercing melee weapon in one hand, the swashbuckler can
      spend 1 panache point to take the fighting defensively action as a
      <em>swift action</em> instead of a <em>standard action</em>. When fighting
      defensively in this manner, the <em>dodge bonus</em> to AC gained from
      that action increases to +4, and the penalty to <em>attack rolls</em> is
      reduced to –2.</p>

      <p><em>Perfect Thrust (Ex): </em>At 15th level, while the swashbuckler has
      at least 1 panache point, she can as a <em>full-round action</em> make a
      perfect thrust, pooling all of her attack potential into a single melee
      attack made with a light or one-handed piercing melee weapon. When she
      does, she makes the attack against the target’s <em>touch</em> AC, and
      ignores all <em>damage reduction</em>.</p>

      <p><em>Swashbuckler’s Edge (Ex):</em> At 15th level, while the
      swashbuckler has at least 1 panache point, she can take 10 on any
      <em>Acrobatics</em>, <em>Climb</em>, <em>Escape Artist</em>, <em>Fly</em>,
      <em>Ride</em>, or <em>Swim</em> check, even while distracted or in
      immediate danger. She can use this ability in conjunction with the
      derring-do deed.</p>

      <p><em>Cheat Death (Ex):</em> At 19th level, whenever the swashbuckler is
      reduced to 0 <em>hit points</em> or fewer, she can spend all of her
      remaining panache to instead be reduced to 1 hit point. She must have at
      least 1 panache point to spend. Effects that kill the swashbuckler
      outright without dealing hit point damage are not affected by this
      ability.</p>

      <p><em>Deadly Stab (Ex):</em> At 19th level, when the swashbuckler
      confirms a <em>critical hit</em> with a light or one-handed piercing melee
      weapon, in addition to the normal damage, she can spend 1 panache point to
      inflict a deadly stab. The target must succeed at a <em>Fortitude</em>
      saving throw or die. The DC of this save is 10 + 1/2 the swashbuckler’s
      level + the swashbuckler’s <em>Dexterity</em> modifier. This is a death
      attack. Performing this deed does not grant the swashbuckler a panache
      point.</p>

      <p><em>Stunning Stab (Ex):</em> At 19th level, when a swashbuckler hits a
      creature with a light or one-handed piercing melee weapon, she can spend 2
      panache points to <em>stun</em> the creature for 1 round. The creature
      must succeed at a <em>Fortitude</em> saving throw (DC = 10 + 1/2 the
      swashbuckler’s level + the swashbuckler’s <em>Dexterity</em> modifier) or
      be <em>stunned</em> for 1 round. Creatures that are immune to critical
      hits are also immune to this effect.</p>
  sources:
    - id: PZO1129
      pages: 56-59
  subType: classFeat
type: feat
