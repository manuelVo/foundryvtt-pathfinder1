import path from "node:path/posix";

import { buildLanguageFiles, buildLanguageFile } from "./help-lang.mjs";
import { ViteLoggerPF } from "./vite-logger.mjs";

import { Watcher, debounce } from "./lib.mjs";

/** @type {import ("vite").ViteDevServer} */
let server;
/** @type {Watcher} */
let watcherJson;
/** @type {Watcher} */
let watcherMD;
/** @type {ViteLoggerPF} */
let logger;

const ac = new AbortController();

/**
 * A plugin that watches the `lang` and `help` directories for changes, triggering a hot reload within Foundry
 *
 * @returns {import("vite").Plugin}
 */
export default function langReload() {
  return {
    name: "lang-hot-reload",
    configureServer(resolvedServer) {
      server = resolvedServer;
    },

    async configResolved(config) {
      logger = new ViteLoggerPF(config.logger);

      // Don't start up watchers if this is not serve mode
      if (config.command !== "serve") return;

      // Set up watcher
      watcherJson = new Watcher("lang", /\.json$/i, { signal: ac.signal });
      watcherMD = new Watcher("help", /\.md$/i, { signal: ac.signal });

      const rebuildLanguage = async (language) => {
        const reloadedLanguage = await buildLanguageFile(language, { logger });

        if ("content" in reloadedLanguage) {
          // Trigger hot reload within dev server/Foundry
          server.ws.send({
            type: "custom",
            event: "hotLangs:update",
            data: reloadedLanguage,
          });

          logger.info(`Hot Reloading ${language}.json`);
        }
      };

      // Debounce each language separately
      const debouncedRebuild = {};

      const fileHandler = async (file) => {
        /** @type {st1ring} */
        let language;
        if (file.endsWith(".md")) {
          language = file.split(path.posix.sep)[1];
        } else if (file.endsWith(".json")) {
          language = path.basename(file, ".json");
        }

        debouncedRebuild[language] ??= debounce(rebuildLanguage, 200);
        debouncedRebuild[language](language);
      };

      watcherJson.on("change", fileHandler);
      watcherMD.on("change", fileHandler);

      watcherJson.watch();
      watcherMD.watch();
    },

    async buildStart() {
      // Trigger a build of language files when building the system
      await buildLanguageFiles({ logger });
    },

    async buildEnd() {
      await ac.abort();
    },
  };
}
