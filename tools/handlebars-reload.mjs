import path from "node:path/posix";
import fsp from "node:fs/promises";
import { ViteLoggerPF } from "./vite-logger.mjs";
import { removePrefix } from "./foundry-config.mjs";

import { Watcher } from "./lib.mjs";

/** @type {import ("vite").ViteDevServer | undefined} */
let server;
/** @type {Watcher} */
let watcher;

const ac = new AbortController();

/**
 * A plugin that watches the `publicDir` for changes to `.hbs` files, triggering a hot reload within Foundry
 *
 * @returns {import("vite").Plugin}
 */
export default function handlebarsReload() {
  let reloadTimer;

  let queue = [];
  function hotReload() {
    server?.ws.send({
      type: "custom",
      event: "hotHandle:update",
      data: { updates: [...queue] },
    });
    queue = [];
  }

  return {
    name: "handlebars-hot-reload",
    configureServer(resolvedServer) {
      server = resolvedServer;
    },

    configResolved(config) {
      // Don't do anything for full builds
      if (config.command !== "serve") return;

      const logger = new ViteLoggerPF(config.logger);

      watcher = new Watcher("public", /\.hbs$/i, { signal: ac.signal });

      // Clean up base dir to determine file placement within Foundry
      const foundryBaseDir = config.base
        .split(path.sep)
        .join(path.posix.sep)
        .replace(/^\/+|\/+$/g, "");

      /**
       * Handle an individual file change, triggering a hot reload within Foundry
       *
       * @param {string} file - The file that changed
       * @returns {Promise<void>}
       */
      const fileHandler = async (file) => {
        if (!file.endsWith("hbs")) return;

        // Transform OS path into Foundry-suitable path
        const filepathUrl = file.replace(/public\//, "").replace(/^\/+|\/+$/g, "");

        const foundryPath = `${removePrefix(foundryBaseDir)}/${filepathUrl}`;

        // Trigger hot reload within dev server/Foundry
        const content = await fsp.readFile(file, { encoding: "utf8" });
        logger.info(`Hot-reloading ${file} as ${foundryPath}`);

        // Batch debounce hot reload
        clearTimeout(reloadTimer);
        queue.push({ file: foundryPath, content });
        reloadTimer = setTimeout(hotReload, 100);

        // Also copy template to `dist` to persist the change
        const distFile = path.join(config.build.outDir, filepathUrl);
        await fsp.copyFile(file, distFile);
        logger.info(`Copied ${file} to ${distFile}`);
      };

      watcher.on("change", fileHandler);
      watcher.watch();
    },

    async buildEnd() {
      ac.abort();
    },
  };
}
