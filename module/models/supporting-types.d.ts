/**
 * Trait data structure.
 *
 * Built from a simple string array in source data.
 */
interface TraitData {
  /**
   * Base data
   */
  base: Array<string>;
  /**
   * Standard types.
   *
   * These are found in relevant configuration or registry.
   */
  standard: Set<string>;
  /**
   * Custom types.
   *
   * Not found in any config or registry.
   */
  custom: Set<string>;
  /**
   * Both standard and custom types.
   */
  readonly total: Set<string>;
  /**
   * Human readable names, if available.
   */
  readonly names: string[];
}
