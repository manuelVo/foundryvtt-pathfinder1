export {};

declare module "./item-message.mjs" {
  /**
   * Item Data in chat message
   */
  interface ItemMessageData {
    /**
     * Item ID
     */
    id: string;
    /**
     * Item name
     */
    name?: string;
    /**
     * Item description
     */
    description: string;
    /**
     * Identified state
     */
    identified: boolean;
  }

  interface ItemMessageModel extends BaseMessageModel {
    /**
     * Actor UUID
     *
     * BUG?: Is this ever different from speaker?
     */
    actor: string;
    /**
     * Template UUID
     */
    template: string;
    /**
     * Item Data
     */
    item: ItemMessageData;
  }
}
