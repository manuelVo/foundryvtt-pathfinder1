interface ItemSelectorOptions {
  /**
   * Actor from which to get items from.
   */
  actor: ActorPF;
  /**
   * Item list to get item from. Used only if actor is undefined.
   */
  items: Item[];
  /**
   * Filtering callback function.
   */
  filterFunc: function;
  /**
   * Allow empty selection (none, as is were).
   *
   * @defaultValue `true`
   */
  empty: boolean;
}
