/**
 * Migration category tracker for {@link MigrationState}
 */
export class MigrationCategory {
  /** @type {import("./migration-state.mjs").MigrationState} */
  state;

  id;
  label;
  isNumber = false;
  processed = 0;
  invalid = 0;
  errors = [];
  current = null;
  total = null;
  ignored = 0;
  completed = false;

  /**
   *
   * @param {string} id - Category ID
   * @param {string} label - Label
   * @param {boolean} isNumber - Is numeric
   * @param {MigrationState} state - State tracker instance
   */
  constructor(id, label, isNumber, state) {
    this.state = state;
    this.id = id;
    this.label = game.i18n.localize(label);
    this.isNumber = isNumber;
    if (isNumber) {
      //this.total = 0;
      this.processed = 0;
    }
  }

  get progress() {
    return this.ignored + this.invalid + this.processed;
  }

  get percentage() {
    if (this.total === 0) return 1;
    return this.progress / this.total;
  }

  get hasTotal() {
    return this.total !== null;
  }

  get progressLabel() {
    return `${pf1.utils.limitPrecision(this.percentage * 100, 1)} %`;
  }

  /**
   * Signal that an entry has started processing.
   *
   * @param {any} entry - Whatever was started processing.
   */
  startEntry(entry) {
    this.current = entry;
    this.state.emit(this, { entry, action: "process", actionState: "start", processing: this.processed + 1 });
  }

  /**
   * Signal that an entry has finished processing.
   *
   * @param {any} entry - Whatever was finished processing with.
   */
  finishEntry(entry) {
    this.current = null;
    this.processed += 1;
    this.state.emit(this, { entry, action: "process", actionState: "finish", processed: this.processed });
  }

  recordError(entry, error) {
    this.errors.push({ entry, error });
  }

  /**
   * Signal that a specific entry was ignored.
   *
   * @param {any} entry - Whatever was ignored.
   */
  ignoreEntry(entry) {
    this.ignored += 1;
    this.state.emit(this, { entry, action: "ignore" });
  }

  /**
   * Add unspecific ignored entries.
   *
   * @param {number} ignored - Number of ignored entries
   */
  addIgnored(ignored) {
    this.ignored += ignored;
    this.state.emit(this, { action: "info", ignored, total: this.total, invalid: this.invalid });
  }

  /**
   * Record total number of items in this category.
   *
   * @param {number} total - Total number of entries in category
   */
  setTotal(total) {
    this.total = total;
    this.state.emit(this, { action: "info", total, ignored: this.ignored, invalid: this.invalid });
  }

  /**
   * Record total number of invalid items in this category.
   *
   * @param {number} total - Total number of invalid entries in the category
   */
  setInvalid(total) {
    this.invalid = total;
    this.state.emit(this, { action: "info", total: this.total, ignored: this.ignored, invalid: this.invalid });
  }

  setProgress(value) {
    this.current = null;
    this.processed = value;
  }

  /**
   * Signal the start of processing this category.
   */
  start() {
    this.completed = false;
    this.state.emit(this, { action: "start" });
  }

  /**
   * Signal the finishing of processing this category.
   */
  finish() {
    this.completed = true;
    this.state.emit(this, { action: "finish" });
  }

  /**
   * Return name of currently processed entry.
   *
   * @remarks
   * - Null if no entry is being processed currently.
   *
   * @type {string|null}
   */
  get currentName() {
    const current = this.current;
    if (!current) return null;

    if (current instanceof ChatMessage) return `${current.timestamp} [${current.id}]`;
    if (current instanceof foundry.abstract.Document) return current.name;
    if (current instanceof CompendiumCollection) {
      if (game.i18n.has(current.metadata.label)) return game.i18n.localize(current.metadata.label);
      return current.metadata.label;
    }
    return null;
  }

  getInvalidEntries() {
    let collection;
    switch (this.id) {
      case "actors":
      case "items":
      case "scenes":
        collection = game[this.id];
        break;
      default:
        return [];
    }

    const results = [];
    for (const id of collection.invalidDocumentIds) {
      results.push({ id, entry: collection.getInvalid(id) });
    }

    return results;
  }

  getErrorEntries() {
    return this.errors;
  }
}
