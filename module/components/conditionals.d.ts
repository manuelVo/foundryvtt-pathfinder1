export {};

declare module "./conditionals.mjs" {
  interface ItemConditional {
    _id: string;
    name: string;
    default?: boolean;
    modifiers?: Collection<ItemConditionalModifier>;
  }

  interface ItemConditionalModifier {
    _id: string;
    formula: string;
    target: string;
    subTarget?: string;
    type: string;
    damageType?: string;
    critical?: ItemChange.Operator;
  }
}
