interface RechargeActorItemsOptions
  extends Exclude<Partial<NonNullable<Parameters<pf1.documents.item.ItemPF["recharge"]>[0]>>, "commit"> {
  /**
   * If false, return update data object instead of directly updating the actor.
   *
   * @defaultValue `true`
   */
  commit?: boolean;
}

/**
 * Raw skill data saved in actor.system.skills
 */
interface SkillData {
  ability: keyof typeof pf1.config.abilities;

  /** Whether Armor Check Penalty applies */
  acp: boolean;

  /** Whether or not this a background skill for the optional Background Skills rule */
  background?: boolean;

  /** Whether or not this is a class skill */
  cs: boolean;

  /** Whether or not this is a custom skill */
  custom?: boolean;

  /** Compendium UUID */
  journal?: string;

  /** Modifier */
  mod: number;

  /**
   * Name
   *
   * @remarks
   * - If not present, name is looked up from {@link pf1.config.skills}
   */
  name?: string;

  rank: number;

  /** Requires training */
  rt: boolean;

  subSkills?: Record<string, SkillData>;
}

/**
 * SkillInfo returned by actor.getSkillInfo()
 *
 * @inheritdoc
 */
interface SkillInfo extends SkillData {
  id: string;

  /**
   * Skill's full name which include's parent's name if applicable
   * e.g. Profession (Sailor)
   */
  fullName: string;

  /**
   * Info for that parent skill, if this is a sub-skill
   */
  parentSkill?: SkillInfo;
}

/**
 * Ability Score
 */
interface AbilityScoreData {
  /**
   * Base value.
   */
  base: number | null;
  /**
   * Modifier derived from `base`.
   */
  baseMod: number;
  /**
   * Modifier with checks only.
   */
  checkMod: number;
  /**
   * Damage.
   */
  damage: number;
  /**
   * Drain.
   */
  drain: number;
  /**
   * Final ability modifier.
   */
  mod: number;
  /**
   * Penalty.
   */
  penalty: number;
  /**
   * Total ability score.
   */
  total: number;
  /**
   * Undrained total ability score.
   */
  undrained: number;
  /**
   * Undrained ability modifier.
   */
  unmod: number;
  /**
   * Manual penalty input.
   */
  userPenalty: number;
  /**
   * User entered value.
   */
  value: number;
}

/**
 * Source info chunk
 *
 * @todo data/handling to changes
 */
interface SourceInfo {
  /**
   * Bonus type
   */
  modifier: string;
  /**
   * Item name or other label
   */
  name: string;
  /**
   * Change operator
   */
  operator: "add" | "set";
  /**
   * Arbitrary type
   */
  type: string;
  /**
   * Change value
   */
  value: number;
  /**
   * Parent change
   */
  change: ItemChange;
}

interface ParsedResistanceEntry {
  type0: { id: string; name: string; data?: object };
  type1?: { id: string; name: string; data?: object };
  operator?: boolean;
  amount: number;
  label: string;
  isDR: boolean;
  isER: boolean;
  custom?: boolean;
}

declare module "./actor-pf.mjs" {
  interface TargetedApplyDamageOptions {
    /**
     * @defaultValue false
     */
    forceDialog?: boolean;
    /**
     * Value to reduce the damage or healing by after ratio has been applied.
     */
    reduction?: number;
    /**
     * @deprecated - Use reduction instead.
     */
    reductionDefault?: number;
    /**
     * @defaultValue false
     */
    asNonlethal?: boolean;
    /**
     * Targets to apply damage to.
     *
     * Has only meaning on the static function.
     */
    targets: Array<Token | Actor>;
    /**
     * Critical multiplier as needed for Wounds & Vigor variant health rule.
     *
     * Set to 0 for non-critical hits.
     *
     * @defaultValue 0
     */
    critMult?: number;
    /**
     * Apply damage to wounds directly instead of vigor, as needed for Wounds & Vigor variant health rule.
     *
     * @defaultValue false
     */
    asWounds?: boolean;
    /**
     * Triggering event, if any.
     */
    event?: Event;
    /**
     * Chat message reference if any.
     *
     * This is to help modules overriding this function, the system does not use it.
     */
    element?: Element;
    /**
     * Chat message reference if any.
     *
     * This is to help modules overriding this function, the system does not use it.
     */
    message?: ChatMessage;
    /**
     * General reference to what triggered this.
     *
     * Value is expected to be an UUID.
     */
    reference?: string;
    /**
     * Individual instances of damage.
     *
     * This is not processed currently by the system, though does affect apply damage dialog presentation.
     */
    instances?: Array<DamageInstance>;
    /**
     * Is this dual dealing? If enabled, healing affects both normal health and nonlethal as magical healing should.
     *
     * @defaultValue false
     */
    dualHeal?: boolean;
  }

  interface ApplyDamageOptions
    extends Omit<TargetedApplyDamageOptions, "targets", "reduction", "reductionDefault", "forceDialog"> {
    /**
     * Ratio
     *
     * Multiplier on the healing/damage value.
     *
     * @defaultValue 1
     */
    ratio?: number;
  }
}
